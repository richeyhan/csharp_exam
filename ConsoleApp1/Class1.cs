﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Class1
    {
        //public enum Country { Korea, China, Japan };

        static void Main(string[] args)
        {
            /*
             Country myCountry = Country.Korea;

            if (myCountry == Country.Korea)
            {
                Console.WriteLine("한국");
            }
            else if (myCountry == Country.Japan)
            {
                Console.WriteLine("일본");
            }
            else if (myCountry == Country.China)
            {
                Console.WriteLine("중국");
            }
            else        //Country에 속하는 항목이 없을 경우
            {
                Console.WriteLine("선택된 나라가 없습니다.");
            }
            */
            /*
            int result = 0;

            for (int i = 1; i <= 10; i++)
            {
                result += i;         //result = result + i; 축약 표현
            }

            Console.WriteLine("for문 1~10 더하기 : {0}", result);
            */
            /*
            int result = 0;

            int startNumber = 1;
            int maxValue = 10;

            while (startNumber <= maxValue)
            {
                result += startNumber;
                startNumber++;        //startNumber = startNumber + 1; 축약 표현
            }

            Console.WriteLine("while문 1~10 더하기 : {0}", result);
            */
            /*
            Foo("123");        //올바르게 처리
            Foo(null);         //ArgumentNullException 발생
            Foo("일이삼");     //Exception 발생
        }

        static void Foo(string data)
        {
            //복수개의 catch문이 올 수도 있다.
            try
            {
                int number = Int32.Parse(data);
                Console.WriteLine("number : {0}", number);
            }
            catch (ArgumentNullException ex)    //입력 인자가 없을 경우
            {
                Console.WriteLine("ArgumentNullException 처리 : {0}", ex.Message);
            }
            catch (Exception ex)                //최상위 예외 형식
            {
                Console.WriteLine("Exception 처리 : {0}", ex.Message);
            }
            */
            int[] array1D = { 1, 2, 3, 4, 5 };

            Console.WriteLine("Rank:{0}, Length:{1}", array1D.Rank, array1D.Length);

            Console.WriteLine("1차원 배열 - foreach");
            //foreach문 사용
            foreach (int value in array1D)
            {
                Console.WriteLine(value);
            }

            Console.WriteLine("1차원 배열 - for");
            //for문 사용
            for (int i = 0; i < array1D.Length; i++)
            {
                Console.WriteLine("array[{0}] : {1}", i, array1D[i]);
            }


        }

    }
}
